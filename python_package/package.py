from requests import get as get_request


def say_hello(name: str = "") -> str:
    r = get_request('https://api.github.com/user', auth=('user', 'pass'))
    data = r.json()
    message = "Hello {name}, GIT says `{msg}`"
    return message.format(name=name, msg=data.get('message', None))


def say_by(name: str = "") -> str:
    return "byby {name}".format(name=name)
