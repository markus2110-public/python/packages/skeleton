
class Helper:
    """

    """
    @staticmethod
    def to_upper(value: str) -> str:
        """

        :param value: str
        :return: str
        """
        return value.upper()
