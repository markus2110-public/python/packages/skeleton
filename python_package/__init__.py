# Used for the 'from package_name import *' import
__all__ = [
    'package', 'helper', 'run'
]

# Used for the 'import python_package' import
from . import package
from . import helper


def run(name="Dummy"):
    value = package.say_hello(name)
    print(helper.Helper.to_upper(value))
