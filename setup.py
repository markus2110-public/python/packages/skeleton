import setuptools

# required packages
requirements = ['requests']
# test packages
test_requirements = requirements + ['flake8', 'coverage', 'tox']
# Dev packages
dev_requirements = test_requirements + []


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="sat.python_package",
    version="0.2.0",
    author="Example Author",
    author_email="author@example.com",
    description="A python package skeleton",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/markus2110-public/python/packages/skeleton",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    extras_require={
        'testing': test_requirements,
        'dev': dev_requirements,
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    scripts=['bin/python_package']
)
