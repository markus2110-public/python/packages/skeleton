# My Python Package Skeleton

https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/

## Install

https://pip.pypa.io/en/latest/cli/pip_install

> For packages on <b>GITLAB</b>,    
> make sure you create an `deploy token`,  
> either on group or project level

### pip command

To install the package from the git package registry us the following: 
```shell
pip install sat.python-package --extra-index-url https://__token__:XMFUs2Ndrk_f9_fegsT9@gitlab.com/api/v4/projects/23044051/packages/pypi/simple
```

To install the package in editable mode (i.e. setuptools “develop mode”) from a VCS url.
> https://pip.pypa.io/en/latest/reference/pip_install/#install-editable
```shell
pip install -e git+http://gitlab.com/markus2110-public/python/packages/skeleton.git@develop#egg=sat.python_package
```
or with `test` dependencies 
```shell
pip install -e git+http://gitlab.com/markus2110-public/python/packages/skeleton.git@develop#egg=sat.python_package[testing]
```
or with `dev` dependencies 
```shell
pip install -e git+http://gitlab.com/markus2110-public/python/packages/skeleton.git@develop#egg=sat.python_package[dev]
```

### requirements file
add the following line to your requirments file
```text
sat.python_package@git+http://gitlab.com/markus2110-public/python/packages/skeleton.git
```
or to install a specific version 
```text
sat.python_package@git+http://gitlab.com/markus2110-public/python/packages/skeleton.git@0.1.0
```
to install packages in `Editable` mode add the following to the file
```text
-e git+http://gitlab.com/markus2110-public/python/packages/skeleton.git#egg=sat.python_package
```

### setup.py
in your setup.py add the following 
```text
setup(
    ...
    install_requires=[
        ...,
        'sat.python_package@git+http://gitlab.com/markus2110-public/python/packages/skeleton.git'
    ]
    ...
) 
```
or to install a specific version
```text
setup(
    ...
    install_requires=[
        ...,
        'sat.python_package@git+http://gitlab.com/markus2110-public/python/packages/skeleton.git@0.1.0'
    ]
    ...
) 
```
---
### some install notes 
> Per default the setup.py installs command line scripts to the users `~/.local/bin` directory.   
> Therefore you need to add this directory to the $PATH variable on UNIX systems,  
> 1. either by adding the following lines manually to `.bash_aliases` which is the recommended way   
> or by adding the lines directly to the `.bashrc` file
>```shell
> .bash_aliases or .bashrc
> # set PATH so it includes user's private bin if it exists
> if [ -d "$HOME/.local/bin" ] ; then
>     PATH="$HOME/.local/bin:$PATH"
> fi
>```
> 2. by running the following command to add the lines automatically to `.bash_aliases`
>```shell
> echo -e "\
> # set PATH so it includes user's private bin if it exists\n\
> if [ -d \"\$HOME/.local/bin\" ] ; then\n\
>     PATH=\"\$HOME/.local/bin:\$PATH\";\n\
> fi" >> ~/.bash_aliases;
>```


### Resources:

##### Build a Python Package Tutorial Link
https://python-packaging.readthedocs.io   
https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/
#### Automate and standardize testing in Python
https://tox.readthedocs.io/en/latest/

### License
https://choosealicense.com/

### Doc ( not used )
https://www.sphinx-doc.org

### Code Quality
https://flake8.pycqa.org/en/latest/
