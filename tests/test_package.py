from unittest import TestCase
import python_package as pp


class Test(TestCase):

    def test_say_hello(self):
        expected = 'Hello Test Name, GIT says `Requires authentication`'
        self.assertEqual(pp.package.say_hello("Test Name"), expected)

    def test_say_by(self):
        self.assertEqual(pp.package.say_by("Test Name"), "byby Test Name")
