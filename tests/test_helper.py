from unittest import TestCase
from python_package.helper import Helper


class Test(TestCase):

    def test_to_upper_for_camelCase(self):
        self.assertEqual(Helper.to_upper("hello Test"), "HELLO TEST")
        self.assertEqual(Helper.to_upper("heLLo"), "HELLO")

    def test_to_upper_for_all_lower(self):
        self.assertEqual(Helper.to_upper("hello test"), "HELLO TEST")
        self.assertEqual(Helper.to_upper("hello"), "HELLO")
