from unittest import TestCase
from unittest.mock import patch

import python_package


class Test(TestCase):

    @patch('builtins.print')
    def test_run(self, mock_print):
        expected = 'HELLO TEST, GIT SAYS `REQUIRES AUTHENTICATION`'
        python_package.run("TEST")
        mock_print.assert_called_with(expected)
